using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Linq;
using System;
using Microsoft.Azure.WebJobs.Host;

namespace TutorlexApi
{

    public class TutorlexSignup
    {
 

        // private TutorDataContext _context;
        // public TutorlexSignup (TutorDataContext context)
        // {
        //     _context = context;
        
        private DocumentClient client;
        private Uri tutorsLink;
        // public TutorlexSignup(TutorStore tutorStore)
        // {
        //     _tutorStore = tutorStore;
        // }
        [FunctionName("CreateTutor")]
        public async static Task<IActionResult> CreateTutor(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "CreateTutor")]HttpRequest req, 
            [CosmosDB(
                databaseName: "sxiqdemo",
                collectionName: "tutors",
                ConnectionStringSetting = "ConnectionString")] DocumentClient client,
                    ILogger log)
        {
        
            var tutorsLink = UriFactory.CreateDocumentCollectionUri("sxiqdemo", "tutors");
            log.LogInformation("Creating a new tutor");
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var input = JsonConvert.DeserializeObject<TutorCreateModel>(requestBody);

            var tutor = new Tutor() 
            { 
                Firstname = input.Firstname,
                Lastname = input.Lastname,
                Email = input.Email,
                Subject = input.Subject,
                Phonenumber = input.Phonenumber,
                Studyscore = input.Studyscore
            
            
            };

            
            await client.CreateDocumentAsync(tutorsLink, tutor);
           
            return new OkObjectResult(tutor);
        }
        


    }
}
