﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Hosting;


namespace TutorlexApi
{
   
    public class Tutor
    {
        public string Id { get; set; } = Guid.NewGuid().ToString("n");
        public DateTime CreatedTime { get; set; } = DateTime.UtcNow;
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phonenumber { get; set; }
        public string Subject { get; set; }
        public int Studyscore { get; set; }

    }

    public class TutorCreateModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phonenumber { get; set; }
        public string Subject { get; set; }
        public int Studyscore { get; set; }
    }

    public class TutorUpdateModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phonenumber { get; set; }
        public string Subject { get; set; }
        public int Studyscore { get; set; }
    }

    // public class TutorTableEntity : TableEntity
    // {
    //     public string Firstname { get; set; }
    //     public string Lastname { get; set; }
    //     public string Email { get; set; }
    //     public string Phonenumber { get; set; }
    //     public string Subject { get; set; }
    //     public int Studyscore { get; set; }

    //     public DateTime CreatedTime { get; set; }
    // }

    // public static class Mappings
    // {
    //     public static TutorTableEntity ToTableEntity(this Tutor tutor)
    //     {
    //         return new TutorTableEntity()
    //         {
    //             PartitionKey = "TUTOR",
    //             RowKey = tutor.Id,
    //             CreatedTime = tutor.CreatedTime,
    //             Firstname = tutor.Firstname,
    //             Lastname = tutor.Lastname,
    //             Email = tutor.Email,
    //             Subject = tutor.Subject,
    //             Studyscore = tutor.Studyscore
    //         };
    //     }

        // public static Tutor TutorReturn(this TutorTableEntity tutor)
        // {
        //     return new Tutor()
        //     {
        //         Id = tutor.RowKey,
        //         CreatedTime = tutor.CreatedTime,
        //         Firstname = tutor.Firstname,
        //         Lastname = tutor.Lastname,
        //         Email = tutor.Email,
        //         Subject = tutor.Subject,
        //         Studyscore = tutor.Studyscore
        //     };
        // }

        // public class TutorDataContext: DbContext 
        // {

        //     public TutorDataContext(DbContextOptions<TutorDataContext> options): base(options) {}

        //     public DbSet < Tutor > Tutors {  
        //     get;  
        //     set;
        //     }  

        // }

       

}
