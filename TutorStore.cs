using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents;
using System.Threading.Tasks;

namespace TutorlexApi
{
   
    public class TutorStore
    {   
        private DocumentClient client;
        private Uri tutorsLink;
        public TutorStore ()
        {
            var uri = new Uri("https://sxdb.documents.azure.com:443/");
            var key = "HYRtE72BzQ5DVDWfzSEjkYbVi59x4SN0vufBnk4B03HOQqt72y5rw2FXQ2HP5fj5HAF8SDIGvi2mn5DbrYRetg==";
            client = new DocumentClient(uri, key);
            tutorsLink = UriFactory.CreateDocumentCollectionUri("sxiqdemo", "tutors");

            
        }

        // public async Task InsertTutor(IEnumerable<Tutor> tutors)
        // {
        //     foreach(var tutor in tutors)
        //     {
        //         await client.CreateDocumentAsync(tutorsLink, tutor);
        //     }
        // }

        public async Task InsertTutor(Tutor tutor)
        {
         
                await client.CreateDocumentAsync(tutorsLink, tutor);
            
        }

         public IEnumerable<Tutor> GetAllTutors()
        {
            var tutors = client.CreateDocumentQuery<Tutor>(tutorsLink)
                                .OrderBy(c => c.Firstname);

            return tutors;
        }
        
    }



}